import os
import cv2
import numpy as np
import torch
import zipfile
import urllib.request

from generator import Generator


def __download_checkpoint(url, full_path):
    urllib.request.urlretrieve(url, full_path)


def __unzip_checkpoint(path, full_path):
    dataset_zip = zipfile.ZipFile(full_path, 'r')
    dataset_zip.extractall(path)


def __load_checkpoint(model, path):
    checkpoints = os.listdir(path)
    checkpoints = [d for d in checkpoints if os.path.isdir(os.path.join(path, d))]
    checkpoints.sort()

    checkpoint_directory = os.path.join(path, checkpoints[-1])

    checkpoint_file = os.path.join(checkpoint_directory, "gen_asian.pth.tar")

    device = "cuda" if torch.cuda.is_available() else "cpu"

    checkpoint = torch.load(checkpoint_file, map_location=device)
    model.load_state_dict(checkpoint["state_dict"])


def get_model():
    url = "https://gitlab.com/glebtutik/european_to_asian_files/-/raw/main/checkpoints/checkpoints.zip"
    path = "./models"
    file_name = "checkpoints.zip"
    full_path = os.path.join(path, file_name)

    # __download_checkpoint(url, full_path)
    __unzip_checkpoint(path, full_path)

    gen_A = Generator()
    __load_checkpoint(gen_A, path)

    return gen_A


def extract_faces(images):
    face_cascade = cv2.CascadeClassifier("./haarcascade/haarcascade_frontalface_default.xml")

    faces = []
    for img in images:
        img = cv2.copyMakeBorder(img, 256, 256, 256, 256, cv2.BORDER_REPLICATE)
        gray_img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        face_coords = face_cascade.detectMultiScale(gray_img, 1.3, 5)

        for coord in face_coords:
            x, y, w, h = coord

            delta = h / 10
            x = int(x - delta * 2.2)
            y = int(y - delta * 3.2)
            w = int(w + delta * 4.5)
            h = int(h + delta * 4.5)

            face = img[y:y + h, x:x + w]

            if face.shape[0] * face.shape[1] > 0:
                face = cv2.resize(face, (256, 256))
                faces.append(face)

    return faces


def postprocessing(tensor):
    image = tensor.cpu().detach().numpy()
    image = image * 0.5 + 0.5
    image = image * 255
    image = np.moveaxis(image, 0, -1)
    return image.astype('uint8')

import cv2
import torch
import numpy as np
import torchvision.transforms as T

from utils import get_model
from utils import postprocessing


def inference(gen_A, face):
    device = "cuda" if torch.cuda.is_available() else "cpu"

    gen_A.to(device)

    transforms = T.Compose(
        [
            T.ToTensor(),
            T.Resize(size=256),
            T.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5]),
        ],
    )

    face = transforms(face)
    face = face.to(device)
    face = gen_A(face)
    face = postprocessing(face)
    
    return face



